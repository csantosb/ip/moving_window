-- * Averager
--
--! @file
--!
--! @brief Input samples averaging over a _g_window_ length window
--!
--! Performs an averaging of an arbitrary size input samples stream over a time
--! windows of size _2**g_window_, being _g_window_ a generic input to the module.
--!
--! @section avg Averaging
--!
--! Averaging is implemented as the difference of the registered samples with its
--! _2**g_window_ shifted version. Result is continuously accumulated to compute a
--! (_2**g_window_ sized) moving window over the samples.
--!
--! @section div Division
--!
--! Its output is left bit shifted by _g_window_ to compute division by _2**g_window_.
--!
--! @class averager

-- * Libraries

library ieee;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

-- * Entity

--! @brief Gets g_window as a generic to compute a 2**g_window length window.
--!
--! @details Rst and clk have standard functionality\n
--! Samples is an arbitrary number of bites input data bus\n\n
--! Average is the sum of all samples in the window, divided by its length,
--! thus the average\n
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...
--!
entity averager is
  generic (g_log2_window : natural);
  port (
    --!\name Global signals
    --!\{
    clk     : in  std_logic;
    rst     : in  std_logic;
    --!\}
    --!\name Input and averaged samples
    --!\{
    samples : in  signed;
    --! length is samples’length
    output  : out signed
   --!\}
    );
end entity averager;

-- * Architecture

--! @brief Implements a simple, general purpose moving averaging.
--!
--! @details It delays the incoming sampling signals, sampling them in parallel.\n
--! Then, takes the difference of the two parallel streams\n
--! accumulating the difference.\n
--! Finally, redimensions the results to compute the division by the window lenght
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...
--!
architecture modular of averager is

  -- ** constants

  --! Window length
  constant c_window_length : natural := 2**g_log2_window;

  -- ** types

  --! Shift register for the incoming samples. Its length is given by the window
  type samples_array is array (c_window_length-1 downto 0) of signed(samples'range);

  -- ** signals
  --
  --! Registered incoming samples
  signal samples_registered : signed(samples'range) := (others => '0');

  --! Shifted incoming samples
  signal samples_shifted : signed(samples'range) := (others => '0');

  --! Signed registered samples
  signal s_samples_registered : signed(samples'length downto 0) := (others => '0');

  --! Signed shifted samples
  signal s_samples_shifted : signed(samples'length downto 0) := (others => '0');

  --! Difference of registered and shifted samples
  signal samples_diff : signed(samples'length downto 0) := (others => '0');

  --! Accumulation of difference samples
  signal samples_accum : signed(samples'left+c_window_length downto 0) := (others => '0');

  --! Shift register
  signal samples_srl : samples_array := (others => (others => '0'));

begin

  -- ** moving window

  -- *** register samples

  --! @brief Register process
  --!
  --! @details Just register incoming samples\n
  --! The register size it’s been fixed during signal declaration\n
  --! Note that it performs signed arithmetic.
  --!
  --! @section sec1 Section1
  --!
  --! All is clear, I guess ...
  --!
  --! @section sec2 Section2
  --!
  --! Don’t know what to say ...

  p_mw_register_samples : process (clk) is
  begin
    if rising_edge(clk) then
      if rst = '1' then
        samples_registered <= (others => '0');
      else
        samples_registered <= samples;
      end if;
    end if;
  end process;

  -- *** shift samples by 2**g_window=c_window_length clock periods

  --! @brief Shift register process
  --!
  --! @details Sum up the result of the difference block\n
  --! The register size it’s been fixed during signal declaration\n
  --! Note that it performs signed arithmetic.
  --!
  --! @section sec1 Section1
  --!
  --! All is clear, I guess ...
  --!
  --! @section sec2 Section2
  --!
  --! Don’t know what to say ...

  p_mw_shift_register : process (clk) is
  begin
    if rising_edge(clk) then
      -- shift register mechanism
      samples_srl(samples_srl'high downto 1) <= samples_srl(samples_srl'high-1 downto 0);
      if rst = '1' then
        samples_srl(samples_srl'low) <= (others => '0');
        samples_shifted              <= (others => '0');
      else
        -- shift register input
        samples_srl(samples_srl'low) <= samples_registered;
        -- shift register output
        samples_shifted              <= samples_srl(samples_srl'high);
      end if;
    end if;
  end process;

  -- *** take difference of registered and shifted samples

  --! @brief Difference process
  --!
  --! @details Computes the difference of delayed and registerd samples\n
  --! The register size it’s been fixed during signal declaration\n
  --! Note that it performs signed arithmetic.
  --!
  --! @section sec1 Section1
  --!
  --! All is clear, I guess ...
  --!
  --! @section sec2 Section2
  --!
  --! Don’t know what to say ...

  p_mw_difference : process (clk) is
  begin
    if rising_edge(clk) then
      if rst = '1' then
        samples_diff <= (others => '0');
      else
        samples_diff <= s_samples_registered - s_samples_shifted;
      end if;
    end if;
  end process;

  -- *** resize

  s_samples_registered <= resize(samples_registered, samples'length+1);
  s_samples_shifted    <= resize(samples_shifted, samples'length+1);

  -- *** accumulate

  --! @brief Accumulate process
  --!
  --! @details Sum up the result of the difference block\n
  --! The register size it’s been fixed during signal declaration\n
  --! Note that it performs signed arithmetic.
  --!
  --! @section sec1 Section1
  --!
  --! All is clear, I guess ...
  --!
  --! @section sec2 Section2
  --!
  --! Don’t know what to say ...

  p_mw_accumulate : process (clk) is
  begin
    if rising_edge(clk) then
      if rst = '1' then
        samples_accum <= (others => '0');
      else
        samples_accum <= samples_accum + samples_diff;
      end if;
    end if;
  end process;

  -- ** divide by 2**g_window

  --!
  --! @class             averager::rtl
  --! @subsection adjstc Adjusted STC mode (5)
  --!                        To be coded
  --!
  --!                        The same interpretation applies as per Adjusted
  --!                        mode case, but the resulting attenuation must be
  --!                        understood as a base value for the STC law.

  --! output size is fixed, that of samples
  output <= resize(samples_accum/c_window_length, samples'length);

end architecture modular;
