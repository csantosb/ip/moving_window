-- * Top
--
--! @file
--! @brief Topmost hierarchy level.
--!
--! Its solely purpose is to wrap the averager module, being used as topmost
--! level under xilinx ise project manager.
--!
--! It declares input signals, define their size and instantiate the averager
--! module.
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...
--!
--! \class top

-- * Libraries

library ieee;
use ieee.std_logic_1164.all;
--! Project package\n\n
use work.top_pkg.all;
use IEEE.numeric_std.all;

-- * Entity
--
--! @brief Top entity.
--!
--! @details Rst and clk have standard functionality.\n
--! Samples is a fixed number of bits input data bus.\n\n
--!
--! Average is the sum of all samples in the window, divided by its length,
--! thus the average.\n
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...

entity top is
  port (clk    : in  std_logic;
        rst    : in  std_logic;
        input  : in  signed(c_width-1 downto 0);
        output : out signed(c_width-1 downto 0));
end entity top;

-- * Architecture

--! @brief Just instantiate the averager module.
--!
--! @details Details of the architecture.\n
--! Simpler that this, you die !.\n
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...

architecture simpler of top is

begin

  -- ** averager

  --! @brief Implements a simple, general purpose moving averaging.
  --!
  --! @details It delays the incoming sampling signals, sampling them in parallel.\n
  --! Then, takes the difference of the two parallel streams\n
  --! accumulating the difference.\n
  --! Finally, redimensions the results to compute the division by the window
  --! lenght
  --!
  --! @section sec1 Section1
  --!
  --! All is clear, I guess ...
  --!
  --! @section sec2 Section2
  --!
  --! Don’t know what to say ...

  DUT : entity work.averager
    generic map (g_log2_window => c_log2_window)
    port map (clk     => clk,
              rst     => rst,
              samples => input,
              output  => output);

end architecture simpler;
