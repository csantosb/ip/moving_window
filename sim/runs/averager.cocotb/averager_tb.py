#!/usr/bin/python
#-*- coding: utf-8 -*-

'''
Compléter la fonction un_des_deux_args_divise_l_autre(a , b) qui
'''

import cocotb
from cocotb.triggers import Timer, FallingEdge, RisingEdge
from cocotb.clock import Clock
from random import uniform, random
from math import floor

# @cocotb.coroutine
# def inputs(dut):

#     yield FallingEdge(dut.clk)

#     dut.MOPm_Run <= round(random())

#     dut.GLOBAL_COUNTER <= floor(uniform(0, 2**32-1))

#     dut.SAMPLES_CH0 <= floor(uniform(0, 2**10-1))
#     dut.SAMPLES_CH1 <= floor(uniform(0, 2**10-1))
#     dut.SAMPLES_CH2 <= floor(uniform(0, 2**10-1))
#     dut.SAMPLES_CH3 <= floor(uniform(0, 2**10-1))
#     dut.SAMPLES_CH4 <= floor(uniform(0, 2**10-1))
#     dut.SAMPLES_CH5 <= floor(uniform(0, 2**10-1))

#     dut.FCFG_Umbral_Look_Over <= floor(uniform(0, 2**8-1))
#     dut.FCFG_Umbral_Det <= floor(uniform(0, 2**8-1))
#     dut.FCFG_Umbral_HIST <= floor(uniform(0, 2**8-1))
#     dut.FCFG_Umbral_3DB <= floor(uniform(0, 2**8-1))
#     dut.FCFG_Pot_Red_3dB <= floor(uniform(0, 2**8-1))
#     dut.FCFG_Pot_Red_3dB_Mrg <= floor(uniform(0, 2**8-1))

#     dut.FCT_IntPA_On <= round(random())

#     dut.FCFG_IntPA_Twait <= floor(uniform(0, 2**8-1))
#     dut.FCFG_IntPA_Orden <= floor(uniform(0, 2**3-1))

#     dut.Inv_Ext <= round(random())
#     dut.Inv_Int <= round(random())
#     dut.Blanking_Int <= round(random())
#     dut.Look_Over <= round(random())
#     dut.Forzar_Pulso_Fin <= round(random())


nbBits = 12

BruitLevel = 4

@cocotb.coroutine
def reset_dut(reset, duration):
    reset <= 1
    yield Timer(duration, units='ns')
    reset <= 0
    reset._log.debug("Reset complete")

@cocotb.coroutine
def noise(samples, clk):
    count = 0
    while 1:
        count += 1
        if count > 50000 and count < 51000:
            samples <= round(2**(nbBits-1)/3 + BruitLevel*uniform(-1, 1))
        else:
            samples <= round(BruitLevel*uniform(-1, 1))
        yield RisingEdge(clk)

@cocotb.test()
def averager_tb(dut):
    """Try accessing the design."""

    dut._log.info("Start running test!")

    reset = dut.rst

    samples = dut.samples

    # clk
    cocotb.fork(Clock(dut.clk, 10, units='ns').start())

    reset_thread = cocotb.fork(reset_dut(reset, 1000))

    samples_thread = cocotb.fork(noise(samples, dut.clk))
    # yield reset_thread.join()

    # yield reset_thread.join()

    # yield Timer(400, units='us')
    # dut.samples <= 30;
    # yield Timer(10, units='us')
    # dut.samples <= 0;
    yield Timer(900, units='us')

    dut._log.info("End running test!")
