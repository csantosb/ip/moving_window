#!/bin/sh

# Copy config file

#   gitlab.com/csantosb/eda-common/Makefile

# as a supporting, common file to all simulations

! [ -e Makefile ] && cp $HOME/Projects/perso/eda-common/common/Makefile.edacommon Makefile

ln -s ../../data/samples/ samples

echo "Done."
