clear

% params
tEnd               = 1000e-6;   % max simulation time
freq               = 100e6;    % sampling frequency
BruitLevel         = 10;
NbChannels         = 1;
nbBits             = 12;       % number of sampling bits
P1Amp              = 2^(nbBits-1); % pulse 1 amplitude
% P2Amp              = P1Amp/2;  % pulse 2 amplitude
P1Init             = 600e-6;     % pulse 1 init time
P1Width            = 10e-6;   % pulse 1 width
% P2Width            = 200e-9;   % pulse 2 width

% time vector
t = 0:1/freq:tEnd;

% video vector
samples = zeros(size(t));  % samples signal
samples(t>P1Init & t<(P1Init+P1Width)) = P1Amp/3; % video signal with pulse1
% samples(t>=(P1Init+P1Width) & t<(P1Init+P1Width+P2Width)) = P2Amp; % video signal with pulse2

% add noise

bruit = rand(size(t))*BruitLevel;
bruit = bruit - BruitLevel/2;
for i=1:100
    bruit = bruit - mean(bruit);
end
samples = samples + bruit;
samples = fix(samples);

% Build an array of 4 videos, one by column
SamplesArray = zeros(length(samples), NbChannels);
for i = 1:NbChannels
    SamplesArray(:, i) = samples';
end

close all
% figure, plot(t, SamplesArray), zoom on

% digitalize videos
% SamplesArray = floor(SamplesArray);

% save videos to disk
fid = fopen('samples.txt', 'w');
dlmwrite(fid, SamplesArray, ' ');
% fclose(fid);

printf("done\n")